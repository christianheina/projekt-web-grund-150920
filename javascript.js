$(document).ready(function (){
	var reminder = new Array();
	var reminderDate = new Array();
	if (localStorage["reminder"] != null){
		reminder = JSON.parse(localStorage["reminder"]);
		reminderDate =JSON.parse(localStorage["reminderDate"]);
	}

	$(function () {
		$('[data-toggle="tooltip"]').tooltip();

		var getId = $('body').attr('id');
		if( getId == 'about'){
			var getTab = $('[href=' + location.hash + ']');
			getTab && getTab.tab('show');
		}
	});

	$('#saveButton').on('click', function(){
		if ($('textarea#comment').val() == ""){
			alert("Please add Your reminder or click Close");
		}else {
			reminder[reminder.length] = $('textarea#comment').val();;
			var dt = new Date();
			reminderDate[reminderDate.length] = dt.getDate() + "/" + dt.getMonth() + "-" + dt.getFullYear() + " " + 
				dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();

			localStorage["reminder"] = JSON.stringify(reminder);
			localStorage["reminderDate"] = JSON.stringify(reminderDate);

			text = $('textarea#comment').val("");
			$('#myModal').modal('hide');
		}

		$('#collapseReminders').empty();
		if (reminder.length >10){
			for (var i=0;i<10;i++) {
			$('#collapseReminders').append('<div class ="row"><div class="col-xs-12"><h4>'+ reminderDate[reminderDate.length-(i+1)] + '</h4><p>' +
			 	reminder[reminder.length-(i+1)] + '</p></div></div>');
			}
		}else{
			for (var i=0;i<reminder.length;i++) {
			$('#collapseReminders').prepend('<div class ="row"><div class="col-xs-12"><h4>'+ reminderDate[i] + '</h4><p>' +
			 	reminder[i] + '</p></div></div>');
			}
		}
	});

	$('#showReminders').on('click', function () {
		$('#collapseReminders').empty();
		if (reminder.length >10){
			for (var i=0;i<10;i++) {
			$('#collapseReminders').append('<div class ="row"><div class="col-xs-12"><h4>'+ reminderDate[reminderDate.length-(i+1)] + '</h4><p>' +
			 	reminder[reminder.length-(i+1)] + '</p></div></div>');
			}
		}else if(reminder.length >0){
			for (var i=0;i<reminder.length;i++) {
			$('#collapseReminders').prepend('<div class ="row"><div class="col-xs-12"><h4>'+ reminderDate[i] + '</h4><p>' +
			 	reminder[i] + '</p></div></div>');
			}
		}
	});

	$('#myTabs a').on('click', function (e) {
		e.preventDefault();
		$(this).tab('show');
	});

	$('.goTab a').on('click', function (event) {
		if (event.target.id == "components"){
			$('#myTabs li:eq(0) a').tab('show');
		}
		if (event.target.id == "jscomponents"){
			$('#myTabs li:eq(1) a').tab('show');
		}
		if (event.target.id == "csschanges"){
			$('#myTabs li:eq(2) a').tab('show');
		}
	});
});